<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace dkit\lock_table\components;

use DateTime;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Expression;

/**
 * 
 */
class LockBehavior extends Behavior {
    /*
     * Time lock
     * @timeLock integer
     */

    /**
     * @var integer the attribute sets the counts minutes blocked record
     * Default 1 minutes
     */
    public $timeLock = 60;
    
    /**
     * @var string the attribute is name owner table or models
     */
    public $nameTable;
    
    /**
     * @var string the attribute is name column ID table
     */
    public $tableColumn;
    
    /**
     * @var string message from flash message if the blocked
     * Set this property false if you do not add flash message
     * Default false
     */
    public $message = false;
    
    /**
     *
     * @var type 
     */
    private $_model = NULL;
    
    

    /**
     * 
     * @return events
     */
    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'unlock'
        ];
    }
    
    /**
     * 
     * @return boolean
     * 
     * Checks if a this record is locked
     * If blocked by the current user, then update lock and return false, else return true 
     */
    public function isLocked() {
        if($this->checkLock()){
            if (!$this->authorLock()) {
                
                if ($this->message){
                    Yii::$app->session->setFlash('danger', $this->message);
                }
                
                return true;
            }else {
                $this->updateLock();
                return false;
            }
        }
        $this->lock();
        return false;
    }
    /**
     * 
     * @return boolean
     * 
     * Checks if a this record is locked
     */
    private function checkLock() {
        if ($this->model&&$this->checkTimeLock()) {
            return true;
        }
        return false;
    }
    /**
     * 
     * @return boolean
     * 
     * return true if this element is still being edited
     * if the blocking time is obsolete, release the lock
     */
    private function checkTimeLock() {
        $now = date('U');
        $time_update = $this->model->time_update;
        $interval = $now - $time_update;
        if ($interval > $this->timeLock*1000) {
            $this->unlock();
            return false;
        }
        return true;
    }
    
    private function authorLock(){
        if($this->model->author_update == Yii::$app->user->id){
            return true;
        }else{
            return false;
        }
    }
    

    /**
     * 
     * @return boolean
     * blocks this owner record
     */
    private function lock() {
        $model = new \dkit\lock_table\models\LockUpdate();
        $model->table_id = $this->owner->{$this->tableColumn};
        $model->name_table = $this->nameTable;
        $model->time_update = date('U');
        $model->author_update = Yii::$app->user->id;
        return $model->save();
    }

    /**
     * 
     * @return boolean
     * update time last blocked
     */
    public function updateLock() {
        if($this->model&&$this->authorLock()){
            $this->model->time_update = date('U');
            $this->model->save();
            return true;
        }
    }

    /**
     * unlock owner record
     */
    private function unlock() {
        $this->model->delete();
    }
    

    /**
     * 
     * @return 
     */
    public function getModel() {
        if ($this->_model == NULL) {
            return $this->_model = \dkit\lock_table\models\LockUpdate::find()->andWhere(['name_table' => $this->nameTable, 'table_id' => $this->owner->{$this->tableColumn}])->one();
        }
        return $this->_model;
    }

}
