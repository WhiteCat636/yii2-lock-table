<?php

namespace dkit\lock_table\assets;

use yii\web\AssetBundle;

class BackendAsset extends AssetBundle
{
    public $sourcePath = '@vendor/dk-it/yii2-lock-table/assets';
//    public $css = [
//
//    ];
    public $js = [
        'js/lockUpdate.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}