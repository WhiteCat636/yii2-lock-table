Lock update table
=================
Lock update table

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dk-it/yii2-lock-table "*"
```

or add

```
"dk-it/yii2-lock-table": "*"
```

to the require section of your `composer.json` file.
90

# Run module migrations

```
$ yii migrate/up --migrationPath=@vendor/dk-it/yii2-lock-table/console/migrations

Usage
-----

Once the extension is installed, simply use it in your Model  by  :

```php

    public function behaviors()
    {
        return [
            'lock'=>[
                'class' => \dkit\lock_table\components\LockBehavior::className(),
                'timeLock'=>5*60, //// time lock in seconds
                'name_table'=>'post', // custom name for identification model or module 
                'table_column'=>'id', //field with a unique table ID
                'message' => 'This page is currently being edited, please wait',
            ]
        ];
    }
```

And add to the controller in the update method

```php

        if($model->isLocked()){
            return $this->redirect(['index']);
        }

```


There are two options for setting:
1. Set a long time in the timeLock option and during this time no one other than the same user can edit this record.

2.The second option is to keep the entry locked to edit only while one user has an open page with editing.
For this option, you need to add an edit file:
    
```php

use dkit\lock_table\assets\BackendAsset;
BackendAsset::register($this);

```

And add to the controller in the update method

```php

        // Check if there is ajax request
        if (Yii::$app->request->isAjax) {
            
            if($model->updateLock()){
                // use Yii's response format to encode output as JSON
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [ 'message'=>'true'];
            }else{
                return $this->redirect(['index']);
            }
        }

```