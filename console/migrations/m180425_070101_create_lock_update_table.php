<?php

use yii\db\Schema;

class m180425_070101_create_lock_update_table extends \yii\db\Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('lock_update', [
            'id' => $this->primaryKey(),
            'name_table' => $this->string(155)->notNull(),
            'table_id' => $this->integer(11)->notNull(),
            'author_update' => $this->integer(11)->notNull(),
            'time_update' => $this->integer(11)->notNull(),
            ], $tableOptions);
                
    }

    public function safeDown()
    {
        $this->dropTable('lock_update');
    }
}
