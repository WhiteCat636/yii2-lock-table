<?php

namespace dkit\lock_table\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "lock_update".
 *
 * @property integer $id
 * @property string $name_table
 * @property integer $table_id
 * @property integer $author_update
 * @property integer $time_update
 */
class LockUpdate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_table', 'table_id', 'author_update', 'time_update'], 'required'],
            [['table_id', 'author_update', 'time_update'], 'integer'],
            [['name_table'], 'string', 'max' => 155]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lock_update';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_table' => 'Name Table',
            'table_id' => 'Table ID',
            'author_update' => 'Author Update',
            'time_update' => 'Time Update',
        ];
    }

}
