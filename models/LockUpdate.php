<?php

namespace dkit\lock_table\models;

use Yii;
use \app\models\base\LockUpdate as BaseLockUpdate;

/**
 * This is the model class for table "lock_update".
 */
class LockUpdate extends BaseLockUpdate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name_table', 'table_id', 'author_update', 'time_update'], 'required'],
            [['table_id', 'author_update', 'time_update'], 'integer'],
            [['name_table'], 'string', 'max' => 155]
        ]);
    }
    
    /**
     * 
     * @return boolean
     * 
     * Checks if a this record is locked
     * If blocked by the current user, then update lock and return false, else return true 
     */
    public function blockedUpdate() {
        if ($this->model) {
            if($this->checkTimeLock()){
                if ($this->model->author_update != Yii::$app->user->id) {
                    if ($this->message){
                        Yii::$app->session->setFlash('danger', $this->message);
                    }
                    return true;
                }else {
                    $this->updateLock();
                    return false;
                }
            }
        }
        $this->lock();
        return false;
    }
    
    /**
     * 
     * @return boolean
     * 
     * return true if this element is still being edited
     * if the blocking time is obsolete, release the lock
     */
    public function checkTimeLock() {
        $now = date('U');
        $time_update = $this->time_update;
        $interval = $now - $time_update;
        if ($interval > $this->timeLock*60*1000) {
            $this->unlock();
            return false;
        }
        return true;
    }
    

    /**
     * 
     * @return boolean
     * blocks this owner record
     */
    private function lock() {
        $model = new \dkit\lock_table\models\LockUpdate();
        $model->table_id = $this->owner->{$this->table_column};
        $model->name_table = $this->name_table;
        $model->time_update = date('U');
        $model->author_update = Yii::$app->user->id;
        return $model->save();
    }

    /**
     * 
     * @return boolean
     * update time last blocked
     */
    private function updateLock() {
        $this->time_update = date('U');
        $this->save();
        return true;
    }

    /**
     * unlock owner record
     */
    private function unlock() {
        $this->delete();
    }  
	
}
